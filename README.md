# NPD Practical Test - Level II

Repositório para a prova prática da seleção de novos membros Nível 02 para o Núcleo de Produção de Dados.

### Informações
* Você deve colocar seu código com as respostas em um repositório privado (no GitHub ou GitLab) e adicionar o usuário @genicleito (genicleito@live.com) como membro com permissão de administrador do repositório assim que você criá-lo. 
* Ao término do teste prático você também pode enviar o link do repositório privado para o email da seleção.
* Você poderá utilizar as linguagens Python e/ou R para resolver as tarefas, no entanto, recomendamos utilizar o PySpark.

#### Observações importantes
* Lembre-se de não compartilhar essa prova prática com ninguém ou pedir para que terceiros a responda por você, ela deverá ser feita única e exclusivamente por você.
* Fique à vontade para fazer pesquisas necessárias (não esqueça de citar as fontes)
* Você pode realizar commits à medida que for resolvendo as tarefas. Iremos analisar suas soluções apenas a partir do término do seu prazo de envio.
* Serão analisados apenas os commits realizados dentro do prazo para solução do teste prático. Commits com horário posterior (ou eventualmente anterior ao recebimento da prova prática) serão desconsiderados.
